/**
 * Example of using libmuse library on android.
 * Interaxon, Inc. 2015
 */

package com.interaxon.test.libmuse;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.interaxon.libmuse.Accelerometer;
import com.interaxon.libmuse.ConnectionState;
import com.interaxon.libmuse.Eeg;
import com.interaxon.libmuse.LibMuseVersion;
import com.interaxon.libmuse.Muse;
import com.interaxon.libmuse.MuseArtifactPacket;
import com.interaxon.libmuse.MuseConnectionListener;
import com.interaxon.libmuse.MuseConnectionPacket;
import com.interaxon.libmuse.MuseDataListener;
import com.interaxon.libmuse.MuseDataPacket;
import com.interaxon.libmuse.MuseDataPacketType;
import com.interaxon.libmuse.MuseManager;
import com.interaxon.libmuse.MusePreset;
import com.interaxon.libmuse.MuseVersion;
import com.parse.Parse;
import com.parse.ParseObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * In this simple example MainActivity implements 2 MuseHeadband listeners
 * and updates UI when data from Muse is received. Similarly you can implement
 * listers for other data or register same listener to listen for different type
 * of data.
 * For simplicity we create Listeners as inner classes of MainActivity. We pass
 * reference to MainActivity as we want listeners to update UI thread in this
 * example app.
 * You can also connect multiple muses to the same phone and register same
 * listener to listen for data from different muses. In this case you will
 * have to provide synchronization for data members you are using inside
 * your listener.
 *
 * Usage instructions:
 * 1. Enable bluetooth on your device
 * 2. Pair your device with muse
 * 3. Run this project
 * 4. Press Refresh. It should display all paired Muses in Spinner
 * 5. Make sure Muse headband is waiting for connection and press connect.
 * It may take up to 10 sec in some cases.
 * 6. You should see EEG and accelerometer data as well as connection status,
 * Version information and MuseElements (alpha, beta, theta, delta, gamma waves)
 * on the screen.
 * alpha_absolute [1..4]0-3
 *alpha_relative[1..4]4-7
 *beta_absolute[1..4]8-11
* beta_relative[1..4]12-15
 *blink    16
 *delta_absolute[1..4]17-20
 *delta_relative[1..4]21-24
 *concentration 1 25
 *mellow1  26
 *8gamma_absolute[1..4]27-30
 *gamma_relative[1..4]31-34
 *theta_absolute[1..4]35-38
 *theta_relative[1..4]39-42
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
public class MainActivity extends Activity implements OnClickListener {
    /**
     * Connection listener updates UI with new connection status and logs it.
     */
   //get all the variables in the following
        double[][] x1 = new double[43][1];

    //Integration Code


    double[][] y1=new double[2][1];
    // ===== NEURAL NETWORK CONSTANTS =====
    //Input 1
    double x1_step_offset[]={-0.579411208629608,-0.589196622371674,-0.736807763576508,-0.54952484369278,0,0,0,0,0,-0.159819006919861,-0.295506298542023,0,0,0,0,0,0,-0.694104492664337,-1.22030508518219,-1.47010207176209,-0.985206723213196,0,0,0,0,0,0,-0.0794289037585258,-0.507396817207336,-0.4595727622509,-0.0765396133065224,0,0,0,0,-0.644164621829987,-1.43093883991241,-1.35110819339752,-0.88575005531311,0,0,0,0};
    double x1_step_gain[] = {1.28247293250245,1.39496922056714,1.28246979542359,1.24760015558178,6.35041584051982,6.06618554580052,8.43554283310195,5.97820053385139,1.55089155010248,1.27803466288261,1.25118672716564,1.52073919101197,3.33174483218316,3.09097106127808,3.22724198049734,2.9929499693921,2,0.784235558386689,0.642241170417306,0.570435675730959,0.688454802233574,2.31356137415806,2.32822562890043,2.11885476723871,2.49301913107878,2,2,1.27558520754767,0.994369677194668,0.977284059344675,1.2549713478188,2.66044967474649,2.78268300130483,2.61367371359197,2.73048690072407,1.04519346000277,0.76448260545368,0.835646610659091,0.994194434360605,4.81072677612982,5.28152383710658,6.25148478770303,6.45142802868478};

    //Layer1
    //5x1
    double[][] b1={{1.708308577121467},{1.9326043612420085},{-0.8378070455079667},{1.9587456763022151},{1.7469098484080408}};
    //5x43
    double[][] IW1_1={{-0.73525724985750662,-0.27500565224761081,-0.58949016174944235,0.57649490114140967,-0.18088551520504653,-0.16373420247748754,-0.20887945397057314,0.68806179776193388,-0.8300723832992426,-0.67058956984708507,-1.1467122400428087,-0.70070881191700762,0.19110639964282791,0.10274594489617812,-0.28115325477843345,-0.64149870245687091,0.053483492606028304,-0.63426486187295583,-0.60263251436753207,0.19870260490917377,0.57265860436828697,-0.49330146739178693,0.93880190279267639,-0.024119547125348154,0.39647814011319066,-0.84761999610404293,-0.81504817974926569,-0.55249523016491708,0.40392348488415875,-0.34289079695027858,0.55838888842043244,0.35583675233972623,-0.36571931981044242,0.48989427629169535,0.57619094158040696,0.81576597419227226,-0.54217227984964345,0.45670947118809679,0.087512315405898766,0.40804167995122498,0.25849448095532773,0.3626288230646455,0.096806534877921055},
            { -0.64554986935477909,0.27404968228382753,0.29350038629904901,-0.26903668700901395,-0.2591746756193275,0.46840748504422813,0.38041419751888428,0.14975795569456954,-0.26370908191752446,-0.37624021141382863,-0.20375961553038802,-0.4776915744115322,0.299620565113699,0.14451556168088631,0.34285030510598635,-0.66369453603865247,-0.10179976228370786,-0.20988617854737912,-1.4465484405323448,0.95956969484371313,-0.85714551128564009,-0.014700660822072555,-0.030433271548969382,-0.50667269647851609,-0.12572799269077889,-0.83922639266202959,1.3935450218969487,0.12778570397349845,1.3907461813777944,-0.35528445974272455,1.0996349409335135,0.50788430597514278,0.64801529038433825,1.2162363794225031,0.9448605652716876,0.45539852284471627,1.0400984246758649,0.40260886159856163,-0.26969674185613124,-0.27086224307542467,-0.26064827494794074,-0.52934416080972468,0.53905105027551758},
            {0.52026929630787544,0.19976309526579114,0.048288534870342882,1.063431208236687,-0.33410059968511752,-0.38988221153520231,-0.52455251447243256,0.43016624216846461,0.61480764852690761,-1.2029556670797235,-0.30247805079126705,0.62618533043459412,-0.73158295881008972,-0.020572907378096739,0.19313583641996851,-0.60054522181956826,-0.12579188154529555,-0.93512857550623407,1.2648520462641502,-1.3627607284783472,0.21146837188286338,0.27512101099543229,-1.2813764947835664,0.31638430364145537,-0.020056231576856931,-0.77145892884793166,-0.62160973141903786,0.48017125545959766,-1.7202114377423279,0.20081266126274039,-0.99553578976828772,0.20845025359266153,0.77453377319162542,-0.021033789935355866,-0.23182399271535184,0.53022590464362174,0.86717311649694095,-0.45437317903751928,-0.26766492875799935,-0.2876848090459177,-0.51498121399701624,-1.2329961226183954,-0.10535577158961505},
            {-0.55864271379060781,0.30597483881052917,-1.0435649961362012,-1.3513104372807925,0.5981192115218682,0.87713810976192153,-0.01851331744642902,0.11945386056940052,-0.58339034823601876,-0.78994170637100403,-1.2207218565285025,-3.2244385890887024,0.67324693211555975,-0.48566122385099458,-0.0087650901792668354,-0.66816849771321596,0.17400328419387739,0.3725989420780737,0.041996213366675708,-0.42259620846783974,-0.1414716045768965,-1.4052860866702519,-0.65926523196909448,-1.6963125317748151,-0.2118260621459839,-0.48004614916827087,1.0869308264174085,-0.91792876938998191,-0.125631665775084,-0.62383333203860192,-1.5570807097547321,-0.39711770417473846,-0.39853756120662848,0.021046588133539979,-0.59961602235455358,0.87896353187704812,-0.19683971204857464,1.1605062723375819,-0.70528450045195701,0.64017241895881916,0.21451670649464999,1.2605927050740187,-0.21973572741160066},
            {0.93609566577737047,-0.18798918669328551,-0.12184875191935052,-0.99329902592725949,0.73832425549194092,-0.19476065354496344,-0.01340640668573575,-0.41273966167817289,-0.0524887235049801,-0.057024857996313856,-1.0765778263394634,-0.81442862209866385,-0.36844831165678882,-0.2648877539351282,-0.11718293352928123,-0.01622280161867547,-0.22084202565614153,0.21421468360293663,0.37035348272549989,-0.33607338350451377,-0.12293468130518743,-0.30542161955764807,0.11047617092450601,0.52709354164457889,-0.38007968591126401,0.32363449472055333,0.041421316113315149,-0.020255668729113933,-0.61239914831800846,-1.2791284935737186,-0.53675929636855957,-0.08094802847783128,-0.85341220895273662,-0.43176564009846208,-0.31214897334758862,0.0027913882300440174,-0.20535029359006066,-0.60836216649630315,-0.45127659765005934,-0.33038007238665607,-0.14643142807951354,0.038437433836662163,-0.41942315649777268}};

    //Layer2
    //2x1
    double[][] b2={{0.15961055165064558},
            {-0.02534248199661298}};
    //2x5
    double[][] LW2_1={{1.5920154198961787,3.7190707976432282,-3.9868681149296878,2.2209388044028757,2.4056464866910945},
            {-2.3611070696077552,-3.8550207088933806,2.6828641785064415,-2.5604690037391102,-1.0872764820240999}};

    //Dimensions
    //size(x1,2) is 1
    int Q= 1;

    // Input 1
    double[][] xp1 = new double[43][1];
    double[][] a1= new double[5][1];
    double[][] a2= new double[2][1];

    double INF=Double.POSITIVE_INFINITY;
    double NAN=Double.NaN;



    class ConnectionListener extends MuseConnectionListener {

        final WeakReference<Activity> activityRef;

        ConnectionListener(final WeakReference<Activity> activityRef) {
            this.activityRef = activityRef;
        }

        @Override
        public void receiveMuseConnectionPacket(MuseConnectionPacket p) {
            final ConnectionState current = p.getCurrentConnectionState();
            final String status = p.getPreviousConnectionState().toString() +
                         " -> " + current;
            final String full = "Muse " + p.getSource().getMacAddress() +
                                " " + status;
            Log.i("Muse Headband", full);
            Activity activity = activityRef.get();
            if (activity != null) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        TextView statusText =
                                (TextView) findViewById(R.id.con_status);
                        statusText.setText(status);
                      //  TextView museVersionText =
                        //        (TextView) findViewById(R.id.version);
                        if (current == ConnectionState.CONNECTED) {
                            MuseVersion museVersion = muse.getMuseVersion();
                            String version = museVersion.getFirmwareType() +
                                 " - " + museVersion.getFirmwareVersion() +
                                 " - " + Integer.toString(
                                    museVersion.getProtocolVersion());
                          //  museVersionText.setText(version);
                        } else {
                          //  museVersionText.setText(R.string.undefined);
                        }
                    }
                });
            }
        }
    }

    /**
     * Data listener will be registered to listen for: Accelerometer,
     * Eeg and Relative Alpha bandpower packets. In all cases we will
     * update UI with new values.
     * We also will log message if Artifact packets contains "blink" flag.
     */
    class DataListener extends MuseDataListener {

        final WeakReference<Activity> activityRef;

        DataListener(final WeakReference<Activity> activityRef) {
            this.activityRef = activityRef;
        }

        @Override
        public void receiveMuseDataPacket(MuseDataPacket p) {
            switch (p.getPacketType()) {
                case EEG:
                    updateEeg(p.getValues());
                    break;
                case ACCELEROMETER:
                    updateAccelerometer(p.getValues());
                    break;
                case ALPHA_RELATIVE:
                    updateAlphaRelative(p.getValues());
                    break;
                case ALPHA_ABSOLUTE:
                    updateAlphaAbsolute(p.getValues());
                    break;
                case BETA_RELATIVE:
                    updateBetaRelative(p.getValues());
                    break;
                case BETA_ABSOLUTE:
                    updateBetaAbsolute(p.getValues());
                    break;
                case GAMMA_RELATIVE:
                    updateGammaRelative(p.getValues());
                    break;
                case GAMMA_ABSOLUTE:
                    updateGammaAbsolute(p.getValues());
                    break;
                case DELTA_RELATIVE:
                    updateDeltaRelative(p.getValues());
                    break;
                case DELTA_ABSOLUTE:
                    updateDeltaAbsolute(p.getValues());
                    break;
                case THETA_RELATIVE:
                    updateThetaRelative(p.getValues());
                    break;
                case THETA_ABSOLUTE:
                    updateThetaAbsolute(p.getValues());
                    break;
                default:
                    break;
            }
        }

        @Override
        public void receiveMuseArtifactPacket(MuseArtifactPacket p) {
            if (p.getHeadbandOn() && p.getBlink()) {
                Log.i("Artifacts", "blink");
                 x1[16][0]=1.0;
               // museDataOnline = new ParseObject("museDataOnline");
               // museDataOnline.put("Blinkdata", "blinking");

                    //museDataOnline.saveInBackground();

            }else
            {
                x1[16][0]=0.0;
            }
            if (p.getHeadbandOn() && p.getJawClench()) {
                Log.i("Artifacts", "Jaw Clench");

                //museDataOnline = new ParseObject("museDataOnline");
                //museDataOnline.put("JawClench", "jaw clenching");

                //museDataOnline.saveInBackground();
            }




        }

        private void updateAccelerometer(final ArrayList<Double> data) {
            Activity activity = activityRef.get();
            if (activity != null) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                    }
                });
            }
        }

        private void updateEeg(final ArrayList<Double> data) {
            Activity activity = activityRef.get();
            if (activity != null) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                         TextView tp9 = (TextView) findViewById(R.id.eeg_tp9);
                         TextView fp1 = (TextView) findViewById(R.id.eeg_fp1);
                         TextView fp2 = (TextView) findViewById(R.id.eeg_fp2);
                         TextView tp10 = (TextView) findViewById(R.id.eeg_tp10);
                         tp9.setText(String.format(
                            "%6.2f", data.get(Eeg.TP9.ordinal())));
                         fp1.setText(String.format(
                            "%6.2f", data.get(Eeg.FP1.ordinal())));
                         fp2.setText(String.format(
                            "%6.2f", data.get(Eeg.FP2.ordinal())));
                         tp10.setText(String.format(
                            "%6.2f", data.get(Eeg.TP10.ordinal())));
                  //      museDataOnline = new ParseObject("EEGData_1");
                    //    museDataOnline.put("EEGData_1", String.format(
//                                "%6.2f", data.get(Eeg.TP9.ordinal())));

  //                      museDataOnline = new ParseObject("EEGData_2");
    //                    museDataOnline.put("EEGData_2", String.format(
      //                          "%6.2f", data.get(Eeg.TP9.ordinal())));

        //                museDataOnline.saveInBackground();
                    }
                });
            }
        }

        private void updateAlphaRelative(final ArrayList<Double> data) {
            Activity activity = activityRef.get();
            if (activity != null) activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    TextView elem1 = (TextView) findViewById(R.id.elem1);
                    TextView elem2 = (TextView) findViewById(R.id.elem2);
                    TextView elem3 = (TextView) findViewById(R.id.elem3);
                    TextView elem4 = (TextView) findViewById(R.id.elem4);
                    elem1.setText(String.format(
                            "%6.2f", data.get(Eeg.TP9.ordinal())));
                    elem2.setText(String.format(
                            "%6.2f", data.get(Eeg.FP1.ordinal())));
                    elem3.setText(String.format(
                            "%6.2f", data.get(Eeg.FP2.ordinal())));
                    elem4.setText(String.format(
                            "%6.2f", data.get(Eeg.TP10.ordinal())));


                    //set the double Array to alpha relative values
                    x1[4][0]=(double)data.get(Eeg.TP9.ordinal());
                    x1[5][0]=(double)data.get(Eeg.FP1.ordinal());
                    x1[6][0]=(double)data.get(Eeg.FP2.ordinal());
                    x1[7][0]=(double)data.get(Eeg.TP10.ordinal());

          /*          museDataOnline = new ParseObject("alpharelative");
                    museDataOnline.put("alpharelative", String.format(
                            "%6.2f", data.get(Eeg.FP1.ordinal())));

                    museDataOnline = new ParseObject("betarelative");
                    museDataOnline.put("betarelative", String.format(
                            "%6.2f", data.get(Eeg.FP2.ordinal())));


                    museDataOnline = new ParseObject("gammarelative");
                    museDataOnline.put("betarelative", String.format(
                            "%6.2f", data.get(Eeg.TP10.ordinal())));
                    museDataOnline.saveInBackground();
*/
                }
            });
        }


        private void updateAlphaAbsolute(final ArrayList<Double> data) {
            Activity activity = activityRef.get();
            if (activity != null) activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {



                    //set the double Array to alpha relative values
                    x1[0][0]=(double)data.get(Eeg.TP9.ordinal());
                    x1[1][0]=(double)data.get(Eeg.FP1.ordinal());
                    x1[2][0]=(double)data.get(Eeg.FP2.ordinal());
                    x1[3][0]=(double)data.get(Eeg.TP10.ordinal());

                }
            });
        }

        private void updateBetaRelative(final ArrayList<Double> data) {
            Activity activity = activityRef.get();
            if (activity != null) activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {



                    //set the double Array to alpha relative values
                    x1[12][0]=(double)data.get(Eeg.TP9.ordinal());
                    x1[13][0]=(double)data.get(Eeg.FP1.ordinal());
                    x1[14][0]=(double)data.get(Eeg.FP2.ordinal());
                    x1[15][0]=(double)data.get(Eeg.TP10.ordinal());

                }
            });
        }

        private void updateBetaAbsolute(final ArrayList<Double> data) {
            Activity activity = activityRef.get();
            if (activity != null) activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {



                    //set the double Array to alpha relative values
                    x1[8][0]=(double)data.get(Eeg.TP9.ordinal());
                    x1[9][0]=(double)data.get(Eeg.FP1.ordinal());
                    x1[10][0]=(double)data.get(Eeg.FP2.ordinal());
                    x1[11][0]=(double)data.get(Eeg.TP10.ordinal());

                }
            });
        }

        private void updateDeltaRelative(final ArrayList<Double> data) {
            Activity activity = activityRef.get();
            if (activity != null) activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {



                    //set the double Array to alpha relative values
                    x1[21][0]=(double)data.get(Eeg.TP9.ordinal());
                    x1[22][0]=(double)data.get(Eeg.FP1.ordinal());
                    x1[23][0]=(double)data.get(Eeg.FP2.ordinal());
                    x1[24][0]=(double)data.get(Eeg.TP10.ordinal());

                }
            });
        }

        private void updateDeltaAbsolute(final ArrayList<Double> data) {
            Activity activity = activityRef.get();
            if (activity != null) activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {



                    //set the double Array to alpha relative values
                    x1[17][0]=(double)data.get(Eeg.TP9.ordinal());
                    x1[18][0]=(double)data.get(Eeg.FP1.ordinal());
                    x1[19][0]=(double)data.get(Eeg.FP2.ordinal());
                    x1[20][0]=(double)data.get(Eeg.TP10.ordinal());

                }
            });
        }

        private void updateGammaRelative(final ArrayList<Double> data) {
            Activity activity = activityRef.get();
            if (activity != null) activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {



                    //set the double Array to alpha relative values
                    x1[31][0]=(double)data.get(Eeg.TP9.ordinal());
                    x1[32][0]=(double)data.get(Eeg.FP1.ordinal());
                    x1[33][0]=(double)data.get(Eeg.FP2.ordinal());
                    x1[34][0]=(double)data.get(Eeg.TP10.ordinal());

                }
            });
        }

        private void updateGammaAbsolute(final ArrayList<Double> data) {
            Activity activity = activityRef.get();
            if (activity != null) activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {



                    //set the double Array to alpha relative values
                    x1[27][0]=(double)data.get(Eeg.TP9.ordinal());
                    x1[28][0]=(double)data.get(Eeg.FP1.ordinal());
                    x1[29][0]=(double)data.get(Eeg.FP2.ordinal());
                    x1[30][0]=(double)data.get(Eeg.TP10.ordinal());

                }
            });
        }

        private void updateThetaRelative(final ArrayList<Double> data) {
            Activity activity = activityRef.get();
            if (activity != null) activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {



                    //set the double Array to alpha relative values
                    x1[39][0]=(double)data.get(Eeg.TP9.ordinal());
                    x1[40][0]=(double)data.get(Eeg.FP1.ordinal());
                    x1[41][0]=(double)data.get(Eeg.FP2.ordinal());
                    x1[42][0]=(double)data.get(Eeg.TP10.ordinal());

                    double[][] xOut=new double[43][1];
                    double nan=Double.NaN;
                    for(int i=0;i<43;i++) {
                      if(String.valueOf(x1[i][0])!=null)
                        {

                        }
                        else
                      {
                          x1[i][0]=0.0;
                      }
                        if(Double.isNaN(x1[i][0]))
                                x1[i][0]=0.0;
                        //      x1[i][0]=0.0;
                Log.d(" check","i "+i+" "+x1[i][0]);
                    }


                    //check the drowsiness
                   double[][] youtput= yOutput(x1);

                    TextView acc_x = (TextView) findViewById(R.id.acc_x);
                    TextView acc_y = (TextView) findViewById(R.id.acc_y);
                    acc_x.setText(String.format("%6.4f", youtput[1][0]));
                    acc_y.setText(String.format("%6.4f", youtput[0][0]));
                    Log.d("youtput","youtput0" + youtput[0][0] +"youtput1" +youtput[1][0]);
                }
            });
        }

        private void updateThetaAbsolute(final ArrayList<Double> data) {
            Activity activity = activityRef.get();
            if (activity != null) activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {



                    //set the double Array to alpha relative values
                    x1[35][0]=data.get(Eeg.TP9.ordinal());
                    x1[36][0]=data.get(Eeg.FP1.ordinal());
                    x1[37][0]=data.get(Eeg.FP2.ordinal());
                    x1[38][0]=data.get(Eeg.TP10.ordinal());

                }
            });
        }

         private double[][] yOutput(double[][] x1){

             //update x1 in a thread showcasing progressbar
             //hangs program code starts
             // ===== NEURAL NETWORK CONSTANTS initialization code=====

             //--- Input 1---
             // x1_step1_xoffset,x1_step1_gain,x1_step1_ymin


             // implement this, if (x1==nan){x1=0}

             // double[][] x1=new double[43][1];//m =43 n=0
             //for (int i = 0; i < 43; i++) {
             //  if(x1[i][0]==NAN){
             //    x1[i][0] = 1;}
             //}
             double[][] x1_step1_xoffset=new double[43][1];//m =43 n=0
             for (int i = 0; i < 43; i++) {
                 x1_step1_xoffset[i][0] = x1_step_offset[i];
             }

             double[][] x1_step1_gain=new double[43][1];//m =43 n=0
             for (int i = 0; i < 43; i++) {
                 x1_step1_gain[i][0] = x1_step_gain[i];
             }

             double[][] x1_step1_ymin=new double[43][1];//m =43 n=0
             for (int i = 0; i < 43; i++) {
                 x1_step1_ymin[i][0] = -1;
             }

             //layer1
             //layer2
             //already declared

             //======Simulation=======
             //dimension declared

             //Input 1
             xp1=mapminmax_apply(x1,x1_step1_gain,x1_step1_xoffset,x1_step1_ymin);

             //Layer1
             //not using repmat since b1 is the same as repmat(b1,1,1),
             // a1 = tansig_apply(repmat(b1,1,Q) + IW1_1*xp1)
             a1= tansig_apply(Matrix.add(b1, Matrix.multiply(IW1_1,xp1)));

             //Layer2
             //a2 = softmax_apply(repmat(b2,1,Q) + LW2_1*a1); not using repmat

             a2= softmax_apply(Matrix.add(b2, Matrix.multiply(LW2_1,a1)));

             //Output1
             // y1=a2;
             // y1= mean(y1,2); same as a2 in this case hence a2 is the answer



             //testing
             double[][] bar =new double[43][1];

             Matrix.printMatrix(xp1);
             //hangs program code ends

             return a2;
         }

        //additonal functions for Matlab

        //additional functions to complete the matlab functionality

        private double[][] mapminmax_apply(double[][] x, double[][] settings_gain, double[][] settings_offset, double[][] settings_ymin) {
            double[][] y= new double[43][1];
            y=bsxfun("minus",x,settings_offset);
            y=bsxfun("times",y,settings_gain);
            y=bsxfun("plus",y,settings_ymin);
            return y;
        }

        private double[][] bsxfun(String symbol, double[][] first, double[][] settings) {

            if(symbol.equals("plus")){
                return( Matrix.add(first,settings));
            }
            else if(symbol.equals("minus")){
                return( Matrix.subtract(first, settings));
            }
            else if(symbol.equals("times")){
                return( Matrix.bMultiply(first, settings));
            }else if(symbol.equals("rdivide")){
                return (Matrix.rDivide(first, settings));
            }
            return first;
        }


        private double[][] tansig_apply(double[][] b) {
            int m=b.length;
            int n =b[0].length;
            double[][] a= new double[m][n];
            //  a = 2 ./ (1 + exp(-2*n)) - 1;

            //generate 2
            double[][] two= Matrix.identity(2,m,n);
            //generate 1
            double[][] one= Matrix.identity(1,m,n);

            //generate 1+exp(-2*n)
            double[][] den= Matrix.add(one, Matrix.expMatrix(Matrix.scalarMultiply(b,-2)));

            a= Matrix.subtract(Matrix.rDivide(two,den),one);


            return a;
        }

        private double[][] softmax_apply(double[][] b) {
            int m = b.length;
            int n=b[0].length;
            double[][]a=new double[m][n];

            //max is hardcoded
            //nmax = max(n,[],1);
            double b1=b[0][0];
            double b2=b[1][0];
            double nmax;
            if(b1>=b2)
            {
                nmax=b1;
            }else
                nmax=b2;

            //convert nmax to matrix
            double[][] nmaxMat= Matrix.identity(nmax,m,n);
            b=bsxfun("minus",b,nmaxMat);
            double[][] numer=Matrix.expMatrix(b);
            double denom= numer[0][0]+ numer[1][0];
            //denom(denom == 0) = 1;
            if(denom==0){denom=1;}
            double[][] denomMat= Matrix.identity(denom,m,n);
            a=bsxfun("rdivide",numer,denomMat);
            return a;
        }
        //helper function for tansig and softmax
        private int repmat(double[][] b1, int i, int q) {
            return i;
        }

        private double[][] mean(double[][] y1, int i) {
            return y1;
        }

        //additional functions end

    }

    private Muse muse = null;
    private ConnectionListener connectionListener = null;
    private DataListener dataListener = null;
    private boolean dataTransmission = true;
   // ParseObject museDataOnline;



    public MainActivity() {


        //parse functions

        // PlayerStats playerStats;
         ParseObject playerOnlineData;
        // Create listeners and pass reference to activity to them
        WeakReference<Activity> weakActivity =
                                new WeakReference<Activity>(this);
        connectionListener = new ConnectionListener(weakActivity);
        dataListener = new DataListener(weakActivity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button refreshButton = (Button) findViewById(R.id.refresh);
        refreshButton.setOnClickListener(this);
        Button connectButton = (Button) findViewById(R.id.connect);
        connectButton.setOnClickListener(this);
        Button disconnectButton = (Button) findViewById(R.id.disconnect);
        disconnectButton.setOnClickListener(this);
        Button pauseButton = (Button) findViewById(R.id.pause);
        pauseButton.setOnClickListener(this);
        Log.i("Muse Headband", "libmuse version=" + LibMuseVersion.SDK_VERSION);


        //mellow and concentration
        x1[25][0]=1.0;
        x1[26][0]=1.0;
        //parse functions

        // Enable Local Datastore.
        //Parse.enableLocalDatastore(this);

        //Parse.initialize(this, "Fdt5jUzaZiYupOqejhCqJ7VnriFb3BhA8Cb1xGD3", "Mr6tkLJjFDlekyhKu5Ya15m52q1f1dv5xmbE3cn4");


    }

    @Override
    public void onClick(View v) {
        Spinner musesSpinner = (Spinner) findViewById(R.id.muses_spinner);
        if (v.getId() == R.id.refresh) {
            MuseManager.refreshPairedMuses();
            List<Muse> pairedMuses = MuseManager.getPairedMuses();
            List<String> spinnerItems = new ArrayList<String>();
            for (Muse m: pairedMuses) {
                String dev_id = m.getName() + "-" + m.getMacAddress();
                Log.i("Muse Headband", dev_id);
                spinnerItems.add(dev_id);
            }
            ArrayAdapter<String> adapterArray = new ArrayAdapter<String> (
                    this, android.R.layout.simple_spinner_item, spinnerItems);
            musesSpinner.setAdapter(adapterArray);
        }
        else if (v.getId() == R.id.connect) {
            List<Muse> pairedMuses = MuseManager.getPairedMuses();
            if (pairedMuses.size() < 1 ||
                musesSpinner.getAdapter().getCount() < 1) {
                Log.w("Muse Headband", "There is nothing to connect to");
            }
            else {
                muse = pairedMuses.get(musesSpinner.getSelectedItemPosition());
                ConnectionState state = muse.getConnectionState();
                if (state == ConnectionState.CONNECTED ||
                    state == ConnectionState.CONNECTING) {
                    Log.w("Muse Headband", "doesn't make sense to connect second time to the same muse");
                    return;
                }
                configure_library();
                /**
                 * In most cases libmuse native library takes care about
                 * exceptions and recovery mechanism, but native code still
                 * may throw in some unexpected situations (like bad bluetooth
                 * connection). Print all exceptions here.
                 */
                try {
                    muse.runAsynchronously();
                } catch (Exception e) {
                    Log.e("Muse Headband", e.toString());
                }
            }
        }
        else if (v.getId() == R.id.disconnect) {
            if (muse != null) {
                muse.disconnect(true);
            }
        }
        else if (v.getId() == R.id.pause) {
            dataTransmission = !dataTransmission;
            if (muse != null) {
                muse.enableDataTransmission(dataTransmission);
            }
        }
    }

    private void configure_library() {
        muse.registerConnectionListener(connectionListener);
        muse.registerDataListener(dataListener,
                                  MuseDataPacketType.ACCELEROMETER);
        muse.registerDataListener(dataListener,
                                  MuseDataPacketType.EEG);
        muse.registerDataListener(dataListener,
                                  MuseDataPacketType.ALPHA_RELATIVE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.ALPHA_ABSOLUTE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.BETA_RELATIVE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.BETA_ABSOLUTE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.GAMMA_RELATIVE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.GAMMA_ABSOLUTE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.DELTA_RELATIVE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.DELTA_ABSOLUTE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.THETA_RELATIVE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.THETA_ABSOLUTE);
        muse.registerDataListener(dataListener,
                                  MuseDataPacketType.ARTIFACTS);

        muse.setPreset(MusePreset.PRESET_14);
        muse.enableDataTransmission(dataTransmission);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
