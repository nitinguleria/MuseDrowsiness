package com.interaxon.test.libmuse;

/**
 * Created by nitinguleria on 15-03-30.
 */
/**
 * Created by niting on 3/29/2015.
 */
public class Matrix {

    static double inf=Double.POSITIVE_INFINITY;

    // return a random m-by-n matrix with values between 0 and 1
    public static double[][] random(int m, int n) {
        double[][] C = new double[m][n];
        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                C[i][j] = Math.random();
        return C;
    }

    // return m-by-n identity matrix I
    public static double[][] identity(double value,int m,int n) {
        double[][] I = new double[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                I[i][j] = value;
            }
        }

        return I;
    }

    // return x^T y
    public static double dot(double[] x, double[] y) {
        if (x.length != y.length) throw new RuntimeException("Illegal vector dimensions.");
        double sum = 0.0;
        for (int i = 0; i < x.length; i++)
            sum += x[i] * y[i];
        return sum;
    }

    // return C = A^T
    public static double[][] transpose(double[][] A) {
        int m = A.length;
        int n = A[0].length;
        double[][] C = new double[n][m];
        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                C[j][i] = A[i][j];
        return C;
    }

    // return C = A + B
    public static double[][] add(double[][] A, double[][] B) {
        int m = A.length;
        int n = A[0].length;
        double[][] C = new double[m][n];
        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                C[i][j] = A[i][j] + B[i][j];
        return C;
    }

    // return C = A - B
    public static double[][] subtract(double[][] A, double[][] B) {
        int m = A.length;
        int n = A[0].length;
        double[][] C = new double[m][n];
        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                C[i][j] = A[i][j] - B[i][j];
        return C;
    }

    // return C = A * B
    public static double[][] multiply(double[][] A, double[][] B) {
        int mA = A.length;
        int nA = A[0].length;
        int mB = B.length;
        int nB = B[0].length;
        if (nA != mB) throw new RuntimeException("Illegal matrix dimensions.");
        double[][] C = new double[mA][nB];
        for (int i = 0; i < mA; i++)
            for (int j = 0; j < nB; j++)
                for (int k = 0; k < nA; k++)
                    C[i][j] += (A[i][k] * B[k][j]);
        return C;
    }

    // matrix-vector multiplication (y = A * x)
    public static double[] multiply(double[][] A, double[] x) {
        int m = A.length;
        int n = A[0].length;
        if (x.length != n) throw new RuntimeException("Illegal matrix dimensions.");
        double[] y = new double[m];
        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                y[i] += (A[i][j] * x[j]);
        return y;
    }


    // vector-matrix multiplication (y = x^T A)
    public static double[] multiply(double[] x, double[][] A) {
        int m = A.length;
        int n = A[0].length;
        if (x.length != m) throw new RuntimeException("Illegal matrix dimensions.");
        double[] y = new double[n];
        for (int j = 0; j < n; j++)
            for (int i = 0; i < m; i++)
                y[j] += (A[i][j] * x[i]);
        return y;
    }
    //element wise multiplication
    public static double[][] bMultiply(double[][] A, double[][] B) {
        int m = A.length;
        int n = A[0].length;
        // if (x.length != m) throw new RuntimeException("Illegal matrix dimensions.");
        double[][] C = new double[m][n];
        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                C[i][j] += (A[i][j] * B[i][j]);

        return C;
    }

    //scalar multiplication
    public static double[][] scalarMultiply(double[][] A, double x) {
        int m = A.length;
        int n = A[0].length;

        double[][] C = new double[m][n];
        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                C[i][j] = (A[i][j] * x);

        return C;
    }
    //element wise division
    public static double[][] rDivide(double[][] A, double[][] B) {
        int m = A.length;
        int n = A[0].length;
        // if (x.length != m) throw new RuntimeException("Illegal matrix dimensions.");
        double[][] C = new double[m][n];
        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++) {
                if(B[i][j]==0)

                    C[i][j]=inf;
                else {
                    C[i][j] += (A[i][j] / B[i][j]);
                }
            }
        return C;
    }
    //print matrix for debugging
    public static void printMatrix(double[][] A){
        int m = A.length;
        int n = A[0].length;
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++) {
                System.out.print(A[i][j] + " ");
            }
            System.out.print("\n");
        }


    }
    //get exponential matrix
    public static double[][] expMatrix(double[][] A) {
        int m = A.length;
        int n = A[0].length;
        double[][] B = new double[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                B[i][j] = Math.exp(A[i][j]);
            }

        }

        return B ;
    }


}